import React from 'react';
import { storiesOf, action, linkTo } from '@kadira/storybook';
import Button from './Button';
import Welcome from './Welcome';

import Employee from '../Employee';
import CompanyContainer from '../CompanyContainer';

// Mock Data
import MockData from '../employees.mock.json';

storiesOf('Welcome', module)
  .add('to Storybook', () => (
    <Welcome showApp={linkTo('Button')}/>
  ));

storiesOf('Button', module)
  .add('with text', () => (
    <Button onClick={action('clicked')}>Hello Button</Button>
  ))
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}>😀 😎 👍 💯</Button>
  ));

storiesOf('Employee Site', module)
  .add('Employee, Vanilla', () => (
    <Employee
      name="Jim Testersen"
      title="Chief Cheesecake Operator"
      image="http://i.pravatar.cc/300"
      backstory="Likes long walks on the floor."
    />
  ))
  .add('Company Container', () => (
    <CompanyContainer onRequestEmployees={() => mockedEmployeeAPIResponse()} />
  ));


// This emulates a 1 sec delay by API, as we can expect
function mockedEmployeeAPIResponse() {
  const deferred = new Promise((resolve) => {
    setTimeout(() => {
      resolve(MockData);
    }, 1000)
  })
  return deferred;
}
